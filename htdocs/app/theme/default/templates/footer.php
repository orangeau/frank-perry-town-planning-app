	<footer>
		<div class="container">
			<div class="links">
				<a href="#">Terms and Conditions &middot;</a>
				<a title="Release 0.0.1" href="http://www.squareweave.com.au" class="sw">Realised by Squareweave</a>
			</div>
			<div>
				<p>Copyright &copy; Perry Town Planning 2012</p>
			</div>
		</div>
	</footer>

	<!-- vendor / library js -->
	<?php ET::script('jquery/jquery-1.7.1.min') ?>

	<?php ET::minscript('bootstrap/js/bootstrap-transition') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-alert') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-modal') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-dropdown') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-scrollspy') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-tab') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-tooltip') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-popover') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-button') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-collapse') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-carousel') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-typeahead') ?>  
	<?php ET::minscript('ss-standard/ss-standard') ?>

	<!-- app javascript -->
	<?php ET::minscript('jquery.scrollto/jquery.scrollto.min'); ?>
	<?php ET::minscript('shuriken') ?>
	<?php ET::minscript('app') ?>
	<?php ET::minscript() ?>

	<script type="text/javascript">$.extend(true, app, <?php EF::raw(Response::get_javascript()); ?>);</script>
	<!-- finally, theme js -->
	<?php ET::script('toys') ?>
	<?php ET::script('map') ?>
</body>
</html>