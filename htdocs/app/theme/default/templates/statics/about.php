<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Perry Town Planning</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="application-name" content="<?php EF::v(Bootstrap::APP_NAME) ?>">
	<meta name="generator" content="Shuriken">
	<meta name="author" content="Squareweave Pty Ltd, $Id$">

	<link rel="shortcut icon" href="<?php ET::image('ico/favicon.ico'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php ET::image('ico/apple_114x114.png'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php ET::image('ico/apple_72x72.png'); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?php ET::image('ico/apple_57x57.png'); ?>">

	<?php ET::minless('bootstrap/less/bootstrap'); ?>
	<?php ET::minless('bootstrap/less/responsive'); ?>
	<?php ET::minless('global'); ?>
	<?php ET::minless('layout'); ?>
	<?php ET::minless('home'); ?>
	<?php ET::minless('about'); ?>
	<?php ET::minless('map'); ?>
	<?php ET::minless('infobox'); ?>
	<?php ET::minless('responsive'); ?>
	<?php ET::minless(); ?>

	<?php ET::style('ss-standard/ss-standard'); ?>
	<?php ET::minstyle(); ?>
	
	<!-- google maps -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfjQnnVz3tVmPmTgT7gWK9QL7T_y5sPfY&amp;sensor=false"></script>
	
	<!-- typekit, yo -->
	<script type="text/javascript" src="//use.typekit.net/poy2ybs.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	<!--[if IE ]><?php ET::style('ie'); ?><![endif]-->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->	
</head>

<body class='<?php ET::bc_str(); ?>'>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">

			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="<?php EF::dest(); ?>">Perry <span>Town Planning</span></a>
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li><a href="<?php EF::dest(); ?>">Home</a></li>
					<li><a href="<?php EF::dest(); ?>map_overview_street">Map</a></li>
					<li class="active"><a href="<?php EF::dest(); ?>about">About</a></li>
					<li><a href="<?php EF::dest(); ?>contact">Contact</a></li>
				</ul>
			</div><!-- /.nav-collapse -->

		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->

<!--
Here are some standard header elements guiz.
<a href="<?php EF::dest('home') ?>">Home</a>
<a href="<?php EF::dest('login') ?>">Log out &raquo;</a>

<?php if (Session::has_messages() || Session::has_errors()): ?>
	<?php if (Session::has_messages()): ?>
		<?php foreach(Session::get_messages() as $msg): ?>
			<?php EF::raw($msg) ?>
		<?php endforeach; ?>

	<?php endif; ?>
	<?php if (Session::has_errors()):?>
			<?php foreach(Session::get_errors() as $msg): ?>
				<?php EF::raw($msg) ?>
			<?php endforeach; ?>
	<?php endif; ?>
<?php endif; ?>
-->

	<section class="static">
		<div class="container">
			<h1>About Perry Town Planning</h1>
			<div class="content about">
				<p class="lead">Specialising in town planning and property consulting.</p>
				<p>Welcome to Perry Town Planning, specialists in all areas of town planning and property consultancy including residential, commercial, industrial and retail planning, applied economic and social research, and project management.</p>
				<p>Established in 1999 by Frank Perry, <a href="http://www.town-planning.com.au/company-profile">Perry Town Planning</a> is one of Melbourne&lsquo;s leading planning and property consultancy firms.</p>
				<p>Our central Melbourne location allows easy access to a range of lawyers, architects, developers, urban planning professionals and the Victorian Civil and Administrative Appeals Tribunal (VCAT).</p>
				<p>At Perry Town Planning we pride ourselves on best-practice industry practices, driven by our professionally-qualified, enthusiastic and knowledgeable staff.</p>
				<p>Over the past decade Perry Town Planning has achieved broad market exposure and extensive commercial and retail experience through our completed work.</p>
				<p>This has included small and large scale retail expansions, medium-density property developments and residential estate rezonings.</p>
				<p>Other projects have included developing promotional advertising programs for major activity centres and planning work for community facilities such as childminding services, retirement villages and sporting facilities.Testing</p>
				<p><a href="http://www.town-planning.com.au/company-profile" class="btn btn-danger btn-large">See more about Perry Town Planning</a></p>
				</div>
				</div>
			</div>
		</div>
	</section>

	<?php $this->display( 'statics/adstrip' );  ?>

<?php $this->display( 'footer' );  ?>