<?php $this->display( 'statics/map_header' );  ?>

	<section class="map">
		<div class="notification">
			<div class="alert alert-success fade in">
				<button data-dismiss="alert" class="close" type="button">&times;</button>
				<strong>Great!</strong> We&lsquo;ve received your inquiry and will get back to you soon</div>
			</div>
		</div>
		<div class="map_wrapper">
			<div class="action action_small">
				<form class="search">
					<input type="text" class="search-query input-block-level" placeholder="eg. '74 Rose St, Fitzroy'">
					<input type="submit" class="submit" value="&nbsp;">
				</form>
			</div>
			<div id="map_canvas" class="gmap">
				<?php $this->display( 'infobox/contact_tab' );  ?>
			</div> <!-- gmap -->
		</div> <!-- map wrapper -->
	</section>

<?php $this->display( 'footer' );  ?>