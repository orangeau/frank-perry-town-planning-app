<?php $this->display( 'header' );  ?>

	<section class="map">
		<div class="map_wrapper">
			<div class="search">
				<form class="form-search">
					<label>Find an address</label>
					<input type="text" class="input-medium search-query" placeholder="Find an address">
					<input type="submit" class="btn btn-large btn-danger" value="Go">
				</form>
			</div> <!-- search -->
			<div id="map_canvas" class="gmap">
				<?php $this->display( 'infobox/measure' );  ?>
			</div> <!-- gmap -->
		</div> <!-- map wrapper -->
	</section>

<?php $this->display( 'footer' );  ?>