<?php $this->display('statics/map_header'); ?>

	<section class="map">
		<div class="map_wrapper">
			<div class="action action_small">
				<form class="search">
					<input type="text" class="search-query input-block-level" placeholder="eg. '74 Rose St, Fitzroy'">
					<input type="submit" class="submit" value="&nbsp;">
				</form>
			</div>

			<div id="large_map" class="gmap">
				<div class="infobox">
					<div class="header">
						<h3>Overview</h3>
						<!-- <a href="#"><i class="icon ss-print"></i> Print</a> -->
						<a href="mailto:foo@townplanning.com?Subject=72%20Rose%20Street,%20Fitzroy&amp;Body=Hi%20,%20click%20here:%20<?php EF::dest('map'); ?>"><i class="icon ss-mail"></i> Send</a>
						<div class="contact">
							<a href="map_contact">Contact <span>Perry Town Planning</span></a>
						</div>
					</div> <!-- header -->
					
					<div class="content">
						<div class="viewport_wrapper">
							<div class="viewport">
								<div id="street_canvas" class="temp_view" style="width: 600px; height: 300px"></div>
							<!-- 	<img  src="<?php Et::image('map/temp_view.jpg'); ?>"> -->
				<!-- 				<div class="address">
									<p>74 Rose Street,<br>Fitzroy 3065<br>Victoria</p>
								</div> -->
							</div>
							<div class="views clearfix">
								<a href="<?php EF::dest(); ?>map_overview_street" class="active"><i class="icon ss-location"></i> Street View</a>
								<a href="<?php EF::dest(); ?>map_overview_aerial"><i class="icon ss-view"></i> Aerial View</a>
							</div>
						</div>
					</div> <!-- content -->
					
					<div class="download">
						<a href="<?php EF::dest(); ?>app/theme/default/design/pdf/74-76-Rose-Street-Fitzroy-Planning-Property-Report.pdf" target="_blank">
							
							<span>Planning Report</span>
							<span class="subtext">Download</span>
						</a>
						<a href="<?php EF::dest(); ?>app/theme/default/design/pdf/74-76-Rose-Street-Fitzroy-Basic-Property-Report.pdf" target="_blank">
							<span>Property Report</span>
							<span class="subtext">Download</span>
						</a>
					</div>
					
					<div class="download purchase">
						<a href="<?php EF::dest(); ?>app/theme/default/design/pdf/74-76-Rose-Street-Fitzroy-Planning-Property-Report.pdf" target="_blank">
							
							<span>Property Title</span>
							<span class="subtext">Purchase</span>
						</a>
						<a href="<?php EF::dest(); ?>app/theme/default/design/pdf/74-76-Rose-Street-Fitzroy-Basic-Property-Report.pdf" target="_blank">
							<span>Suburb Report</span>
							<span class="subtext">Purchase</span>
						</a>
					</div>
					
					<div class="tabs">
						<a class="active" href="map_overview_street">Overview</a>
						<a href="map_schemes">Schemes</a>
						<a href="map_measure">Measure</a>
					</div>
				</div> <!-- infobox -->
			</div> <!-- gmap -->
		</div> <!-- map wrapper -->
	</section>

	<footer>
		<div class="container">
			<div class="links">
				<a href="#">Terms and Conditions &middot;</a>
				<a href="http://www.squareweave.com.au" class="sw">Realised by Squareweave</a>
			</div>
			<div>
				<p>Copyright &copy; Perry Town Planning 2012</p>
			</div>
		</div>
	</footer>

	<!-- vendor / library js -->
	<?php ET::script('jquery/jquery-1.7.1.min') ?>

	<?php ET::minscript('bootstrap/js/bootstrap-transition') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-alert') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-modal') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-dropdown') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-scrollspy') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-tab') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-tooltip') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-popover') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-button') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-collapse') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-carousel') ?>
	<?php ET::minscript('bootstrap/js/bootstrap-typeahead') ?>  
	<?php ET::minscript('ss-standard/ss-standard') ?>

	<!-- app javascript -->
	<?php ET::minscript('shuriken') ?>
	<?php ET::minscript('app') ?>

	<!-- finally, theme js -->
	<?php //ET::script('toys') ?>
	<?php ET::script('map') ?>
	<?php ET::minscript() ?>

	<script>
jQuery(document).ready(function($) {
	map.initialize_streetview();
});
</script>
	<script type="text/javascript">$.extend(true, app, <?php EF::raw(Response::get_javascript()); ?>);</script>
</body>
</html>