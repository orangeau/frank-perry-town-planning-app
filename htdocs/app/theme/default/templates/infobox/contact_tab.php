<!-- overview for infobox -->

<div class="infobox">

	<div class="header">
		<h3>Contact Us</h3>
		<div class="contact">
			<a href="map_contact">Contact <span>Perry Town Planning</span></a>
		</div>
	</div> <!-- header -->
	
	<div class="content">
		<div class="viewport_wrapper">
			<div class="viewport">
				<form class="form-horizontal contact_form">
					<div class="control-group">
						<label class="control-label" for="inputEmail">Email Address</label>
						<div class="controls">
							<input type="text" id="inputEmail" placeholder="name@mail.com">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputSubject">Subject</label>
						<div class="controls">
							<input type="text" id="inputSubject">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputMessage">Message</label>
						<div class="controls">
							<textarea type="text" id="inputMessage"></textarea>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-large btn-danger" value="Send">
						</div>
					</div>
				</form>
			</div> <!-- viewport -->
			<div class="viewport contact_details">
				<ul class="unstyled">
					<p class="lead"><i class="icon ss-location"></i> Perry Town Planning</p>
					<li>Level 13, 30 Collins Street</li>
					<li>Melbourne VIC 3000</li>
					<li>Ph: 03 9662 1999</li>
					<li>Fax: 03 9662 2044</li>
				</ul>
				<img src="<?php Et::image('../../design/images/map/infobox/map.jpg'); ?>">
			</div>
		</div>
	</div> <!-- content -->
	
	
	<div class="tabs">
		<a href="map_overview_street">Overview</a>
		<a href="map_schemes">Schemes</a>
		<a href="map_measure">Measure</a>
	</div>
	
</div> <!-- infobox -->