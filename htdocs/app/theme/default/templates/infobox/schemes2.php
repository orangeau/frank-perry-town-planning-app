<!-- overview for infobox -->

<div class="infobox">

	<div class="header">
		<h3>Planning Schemes</h3>
		<div class="contact">
			<a href="map_contact">Contact <span>Perry Town Planning</span></a>
		</div>
	</div> <!-- header -->
	
	<div class="content">
		<div class="viewport_wrapper">
			<div class="viewport">
				<p class="protip">Click on an area on the map</p>
				<a href="http://planningschemes.dpcd.vic.gov.au/aaCombinedPDFs/Murrindindi_PS_Ordinance.pdf" target="_blank"><img class="temp_view" src="<?php Et::image('map/schemes2.jpg'); ?>"></a>
			</div>
			<div class="views clearfix">
				<a href="map_schemes">Metropolitan Map</a>
				<a href="map_schemes2" class="active">Rural Map</a>	
			</div>
		</div>
	</div> <!-- content -->
	
	<div class="tabs">
		<a href="map_overview_street">Overview</a>
		<a class="active" href="map_schemes">Schemes</a>
		<a href="map_measure">Measure</a>
	</div>
	
</div> <!-- infobox -->