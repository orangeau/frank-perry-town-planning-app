<!-- overview for infobox -->

<div class="infobox">
	<div class="header">
		<h3>Measure property</h3>
		<div class="contact">
			<a href="map_contact">Contact <span>Perry Town Planning</span></a>
		</div>
	</div> <!-- header -->
	
	<div class="content">
		<div class="viewport_wrapper">
			<div class="viewport">
				<p class="protip">Click on corners of property to highlight</p>
				<div id="aerial_canvas" class="temp_view" style="width: 600px; height: 300px"></div>
			</div>
			<div class="measurements clearfix">
				<div class="area measurement">
					<label>Area</label>
					<input type="text" value="184.34 m&#178;">
				</div>
				<div class="perimeter measurement">
					<label>Perimeter</label>
					<input type="text" value="128.32 m&#178;">
				</div>
			</div>
		</div>
	</div> <!-- content -->

	<div class="tabs">
		<a href="map_overview_street">Overview</a>
		<a href="map_schemes">Schemes</a>
		<a class="active" href="map_measure">Measure</a>
	</div>
	
</div> <!-- infobox -->