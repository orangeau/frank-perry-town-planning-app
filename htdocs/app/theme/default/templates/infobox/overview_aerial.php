<div class="infobox">
	<div class="header">
		<h3>Overview</h3>
		<a href="#"><i class="icon ss-print"></i> Print</a>
		<a href="#"><i class="icon ss-mail"></i> Send</a>
		<div class="contact">
			<a href="map_contact">Contact <span>Perry Town Planning</span></a>
		</div>
	</div> <!-- header -->
	
	<div class="content">
		<div class="viewport_wrapper">
			<div class="viewport">
				<div id="aerial_canvas" class="temp_view" style="width: 600px; height: 300px"></div>
			<!-- 	<img  src="<?php Et::image('map/temp_view.jpg'); ?>"> -->
<!-- 				<div class="address">
					<p>74 Rose Street,<br>Fitzroy 3065<br>Victoria</p>
				</div> -->
			</div>
			<div class="views clearfix">
				<a href="<?php EF::dest(); ?>map_overview_street"><i class="icon ss-location"></i> Street View</a>
				<a href="<?php EF::dest(); ?>map_overview_aerial" class="active"><i class="icon ss-view"></i> Aerial View</a>
			</div>
		</div>
	</div> <!-- content -->
	
	<div class="download">
		<a href="<?php EF::dest(); ?>app/theme/default/design/pdf/74-76-Rose-Street-Fitzroy-Planning-Property-Report.pdf" target="_blank">
			<span>Planning Report</span>
			<span class="subtext">Download</span>
		</a>
		<a href="<?php EF::dest(); ?>app/theme/default/design/pdf/74-76-Rose-Street-Fitzroy-Basic-Property-Report.pdf" target="_blank">
			<span>Property Report</span>
			<span class="subtext">Download</span>
		</a>
	</div>
	
	<div class="tabs">
		<a class="active" href="map_overview_street">Overview</a>
		<a href="map_schemes">Schemes</a>
		<a href="map_measure">Measure</a>
	</div>
</div> <!-- infobox