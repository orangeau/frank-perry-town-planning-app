<?php $this->display( 'header' );  ?>

	<section class="hero">
		<div class="container">
			<h1 class="intro">Easily search for property overviews</h1>
			<div class="action">
				<form action="map_overview_street" method="GET" class="search">
					<input type="text" class="search-query input-block-level" placeholder="Type address">
					<input type="submit" class="submit" value="&nbsp;">
				</form>
			</div> <!-- action -->
		</div>
	</section> <!-- hero -->
	
	<div id="js_nav" class="navbar menu js_nav">
		<div class="navbar-inner">
			<ul class="nav">
				<li><a href="#reports" class="js_scrollto"><i class="icon ss-file"></i> Reports</a></li>
				<li><a href="#schemes" class="js_scrollto"><i class="icon ss-map"></i> Schemes</a></li>
				<li><a href="#measure" class="js_scrollto"><i class="icon ss-crop"></i> Measure</a></li>
			</ul>
		</div>
	</div>
	
	<section class="sales">
		<div class="item" >
			<div class="container" id="reports">
				<div class="row">
					<div class="span7">
						<div class="description">
							<h1><a  class="robotfromfuture"></a>Planning and property reports</h1>
							<img class="underline" alt="Planning and property reports" src="<?php Et::image('home/underline.jpg'); ?>">
							<p class="lead">Download planning and property reports wherever you are.</p>
							<p>A link to a PDF verison of the planning property report for the selected property is easily accessible. These reports will be available for Victorian properties.</p>
						</div>
					</div>
					<div class="span5">
						<img class="flat_image" alt="Planning and property reports" src="<?php Et::image('home/screens/overview.png'); ?>">
					</div>
				</div>
				<hr>
			</div>
		</div> <!-- item -->

		<div class="item" >
			<div class="container" id="schemes">
				<div class="row">
					<div class="span5">
						<img class="flat_image" alt="Access to planning schemes" src="<?php Et::image('home/screens/scheme.png'); ?>">
					</div>
					<div class="span7">
						<div class="description">
							<h1><a  class="robotfromfuture"></a>Access to planning schemes</h1>
							<img class="underline" alt="Access to planning schemes" src="<?php Et::image('home/underline.jpg'); ?>">
							<p class="lead">View a map of Planning Schemes for your region.</p>
							<p>To calculate the area of the property the user must form a polygon around the area of the property by clicking on each vertex of the property’s border. The tool will then calculate the area and perimeter of the property.</p>
						</div>
					</div>
				</div>
				<hr>
			</div>
		</div> <!-- item -->
		
		<div class="item " >
			<div class="container" id="measure">
				<div class="row">
					<div class="span6">
						<div class="description">
							<h1><a  class="robotfromfuture"></a>Measure areas and perimeters</h1>
							<img class="underline" alt="Measure areas and perimiters" src="<?php Et::image('home/underline.jpg'); ?>">
							<p class="lead">Using a customised tool, measure the area and perimeter of any property.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>
					<div class="span5 offset1">
						<img class="flat_image"  alt="Measure areas and perimiters" src="<?php Et::image('home/screens/measure.png'); ?>">
					</div>
					<hr>
				</div>
			</div>
		</div> <!-- item -->
		
		<!--
<div class="item">
			<div class="container">
			<div class="row">
				<div class="span5">
					<img src="<?php Et::image('home/screens/overview.png'); ?>">
				</div>
				<div class="span6 offset1">
					<div class="description">
						<h1>View any property</h1>
						<img class="underline" src="<?php Et::image('home/underline.jpg'); ?>">
						<p class="lead">Searching for any address will show a front and aerial view of the property.</p>
						<p>The Planning Schemes tab will allow the user to see maps of Metropolitan and Rural Vic- toria which they can then click on their location to link through to the corresponding Ordi- nance for that scheme. Planning scheme information is currently available for the Victorian region.</p>
					</div>
				</div>
			</div>
		</div> 
-->
	</section> <!-- sales -->

	<?php $this->display( 'statics/adstrip' ); ?>
<?php $this->display( 'footer' ); ?>