var map = {
	initialize : function() {
		var rosest = new google.maps.LatLng(-37.795965,144.977928);
		var mapOptions = {
			center: rosest,
			zoom: 16,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		
		var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		// var marker = new google.maps.Marker({
		// 	position: rosest,
		// 	title:"74 Rose Street, Fitzroy"
		// });

		// marker.setMap(map);
	},

	initialize_streetview : function() {
		var rosest = new google.maps.LatLng(-37.795858, 144.977862);
		var panoramaOptions = {
			position: rosest,
			pov: {
				heading: 165,
				pitch:0,
				zoom:1
			}
		};
		var myPano = new google.maps.StreetViewPanorama(document.getElementById('street_canvas'), panoramaOptions);
		myPano.setVisible(true);
	},

	initialize_aerial : function() {
		var rosest = new google.maps.LatLng(-37.795965,144.977928);
		var mapOptions = {
			center: rosest,
			zoom: 20,
			mapTypeId: google.maps.MapTypeId.SATELLITE
		};
		
		var map = new google.maps.Map(document.getElementById('aerial_canvas'), mapOptions);
		var marker = new google.maps.Marker({
			position: rosest,
			title:"74 Rose Street, Fitzroy"
		});

		marker.setMap(map);
	}
}
