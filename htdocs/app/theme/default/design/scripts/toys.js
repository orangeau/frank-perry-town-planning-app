// Squareweave designers does some fun toys in here
// They're not real javascript, they're FAKE javascripts that have no maths.
$.extend(true, app, { toys: {

	setup_nav: function()
	{
		// elements
		var nav = $('.js_nav');
		var top = $('.js_top');

		var nav_orig_y = nav.position().top;

		// vars
		var window_y = 0;
		var nav_ul_visible = false;
		
		// nav.find('h3').stop().css({'opacity' : '0'});
		$(window).scroll(function(){
			window_y = $(window).scrollTop()+80;
			// nav
			if(window_y >= nav_orig_y)
			{
				nav.removeClass('absolute');
				nav.addClass('affixed');
			}
			else if(window_y < nav_orig_y)
			{
				nav.removeClass('affixed');
				nav.addClass('absolute');
			}
		}).trigger('scroll');
	}

}});

jQuery(document).ready(function($) {
	/* 
	Love is from here:
	http://twitter.github.com/bootstrap/javascript.html
	*/
	app.toys.setup_nav();
	$('.dropdown-toggle').dropdown();
	// $('#navbar').scrollspy();
	$(".alert").alert();
	$('.js_tooltip').tooltip({}); /* <a href="#" class="js_tooltip" rel="tooltip" data-original-title="some text in the tip"> */
	// $('.js_nav').scrollspy();
	// 
	// 

	$('.js_scrollto').click(function(e) {
		e.preventDefault();
		$.scrollTo($($(e.currentTarget).attr('href')), 300,{offset:+40});
	});
	
	/* placeholder fix ie7 and 8 */
	$('input[placeholder]').each(function(){  
        
        var input = $(this);        
        
        $(input).val(input.attr('placeholder'));
                
        $(input).focus(function(){
             if (input.val() == input.attr('placeholder')) {
                 input.val('');
             }
        });
        
        $(input).blur(function(){
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        });
    });
	
});